import React, { Component} from 'react';
import PropTypes from 'prop-types';

export class Question extends Component {

    render () {
        let {textQuestion, changeCountTrue, changeCountQuestion, children} = this.props;
        return (
            <div>
                <p> 
                  {textQuestion}  
                </p>
                {
                    React.Children.map(children,
                        (ChildrenItem)=>{
                            if (ChildrenItem.props.correct !== undefined) {
                                return React.cloneElement(ChildrenItem,{
                                   // textAnswer: ChildrenItem.props.textAnswer,
                                    afterAnswer: changeCountTrue
                                })
                            }
                            else {
                                return React.cloneElement(ChildrenItem,{
                                //    textAnswer: ChildrenItem.props.textAnswer,
                                    afterAnswer: changeCountQuestion
                                })
                            }
                    })
                }
            </div>

        )

    }
}


export const Answer = ({textAnswer, afterAnswer}) => {
    
            return (    
                <div>
                    <button onClick={afterAnswer}>
                        {textAnswer}
                    </button> 
                </div>
    
            )
    
        }


Question.propTypes = {
    textQuestion: PropTypes.string.isRequired,
    changeCountTrue: PropTypes.func.isRequired,
    changeCountQuestion: PropTypes.func.isRequired,
    children: PropTypes.array.isRequired
};