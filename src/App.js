import React, { Component } from 'react';
import './App.css';
import {Question, Answer} from './Question'

class App extends Component {

  state = {
    countTrue: 0,
    countQuestion: 0,
    questions: [
                {textQuestion:"Что кушают на завтрак бегемотики?",
                 correct:1,
                 answers: ["Других бегемотиков","Детишек","Взрослых"]
                },
                {textQuestion:"Что кушают на завтрак тигрики?",
                 correct:1,
                 answers: ["То что, даст мамочка","Своих братишек","Мамочку"]
                },
                {textQuestion:"Что кушают на завтрак пандочки?",
                 correct:2,
                 answers: ["Канабис","Травку","Посетителей"]
                },
                {textQuestion:"Что кушают на завтрак дельфинчики?",
                 correct:3,
                 answers: ["Хлорку","Детишек","Кильку"]
                }
               ]
  }


  changeCountQuestion = () => {
    this.setState({countQuestion:++this.state.countQuestion})
  }

  changeCountTrue = () => {
    this.setState({countTrue:++this.state.countTrue})
    this.changeCountQuestion();
  }
 
  outputAll = () => {
    let {countQuestion, questions} = this.state;  
    if (countQuestion > questions.length - 1) {
      return (<p>Ваш результат {this.state.countTrue} правильных ответа.</p>)
    }
    else return (   
         
            <Question textQuestion={questions[countQuestion].textQuestion}
                changeCountTrue={this.changeCountTrue}
                changeCountQuestion={this.changeCountQuestion}
            >
                {       
                  questions[countQuestion].answers.map((item)=>{
                    let indexItem = questions[countQuestion].answers.indexOf(item);
                    if (indexItem === (questions[countQuestion].correct -1))
                    {
                      return (
                        <Answer key={indexItem} correct={true}
                        textAnswer={item}
                      />
                      )
                      
                    } else return (<Answer key={indexItem} textAnswer={item}/>) 
                  })

                }
          </Question>
        )
    


  }


  render() {
    return (
      <div className="App">
            {
            this.outputAll()   
            }      
      </div>
    );
  }
 }


export default App;
